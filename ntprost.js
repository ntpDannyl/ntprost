import Cookie from "./cookie.js"

const $ = window.$;
const $nameInput = $('#name-input')
const $gameCard = $('#game-card')
const $startCard = $('#start-card')
const $names = $('#names')
const $startButton = $('#start_button')
const $cardTitle = $('#card-title')
const $cardText = $('#card-text')
const $html = $('html');
const names = [];
const players = {};
const colorpairs = [
    ['#F96167', '#FCE77D'],
    ['#F9D342', '#292826'],
    ['#DF678C', '#3D155F'],
    ['#CCF381', '#4831D4'],
    ['#4A274F', '#F0A07C'],
    ['#EF5455', '#2B3252'],
    ['#FFF748', '#3C1A5B'],
    ['#2F3C7E', '#FBEAEB'],
    ['#8BD8BD', '#243668'],
    ['#141A46', '#EC8B5E'],
    ['#FFFFFF', '#8AAAE5'],
    ['#295F2D', '#FFE67C'],
    ['#161B21', '#F4A950'],
    ['#EB2188', '#080A52'],
    ['#4A171E', '#E2B144'],
    ['#D3202C', '#F7F7F9'],
    ['#358597', '#F4A896'],
    ['#E7D045', '#A04EF6'],
    ['#683c68', '#DDC6B6'],
    ['#AA96DA', '#C5FAD5'],
    ['#AA96DA', '#FFFFD2'],
    ['#FFFFD2', '#AA96DA'],
    ['#FFFFD2', '#529566'],
    ['#234E70', '#FBF8BE'],
    ['#FFE8F5', '#8000FF'],
    ['#191919', '#ffbe77'],
    ['#CC313D', '#F7C5CC'],
    ['#E2D3F4', '#013DC4'],
    ['#533549', '#F6B042'],
    ['#99F443', '#EC449B'],
    ['#050505', '#E6E7E8'],
    ['#EE4E34', '#FCEDDA'],
    ['#072C50', '#FDF5A6'],
    ['#96351E', '#DBB98F'],
    ['#E2D1F9', '#317773'],
    ['#00539C', '#EEA47F'],
    ['#101820', '#FEE715'],
    ['#990011', '#FCF6F5'],
    ['#8AAAE5', '#FFFFFF'],
    ['#FF69B4', '#00FFFF'],
    ['#FCEDDA', '#EE4E34'],
    ['#ADD8E6', '#00008b'],
    ['#89ABE3', '#EA738D'],
    ['#3A6B35', '#E3B448'],
    ['#CBD18F', '#3A6B35'],
    ['#FFA351', '#EED971'],
    ['#8A307F', '#79A7D3'],
    ['#6883BC', '#8A307F'],
    ['#FC766A', '#783937'],
    ['#783937', '#F1AC88'],
    ['#2C5F2D', '#97BC62'],
    ['#2BAE66', '#FCF6F5'],
    ['#FFE77A', '#2C5F2D'],
    ['#7A2048', '#408EC6'],
    ['#1E2761', '#408EC6'],
    ['#B85042', '#E7E8D1'],
    ['#A7BEAE', '#B85042'],
    ['#C7395F', '#DED4E8'],
    ['#EDCBD2', '#80C4B7'],
    ['#3B5BA5', '#E87A5D'],
    ['#E87A5D', '#F3B941'],
    ['#3B5BA5', '#F3B941'],
    ['#678CEC', '#D49BAE'],
    ['#BBCB50', '#678CEC'],
    ['#4AAFD5', '#E7A339'],
    ['#E26274', '#F9EC7E'],
    ['#E3CCB2', '#E26274'],
    ['#B2456E', '#FBEAE7'],
    ['#552619', '#FBEAE7'],
    ['#FBEAE7', '#552619'],
    ['#FBEAE7', '#B2456E'],
    ['#EDF4F2', '#31473A'],
    ['#7C8363', '#EDF4F2'],
    ['#31473A', '#EDF4F2'],
    ['#404041', '#D3CAE2'],
    ['#F6EDE3', '#404041'],
    ['#E1E5EB', '#E59462'],
    ['#EDCD44', '#DC3E26'],
    ['#96FFBD', '#1803A5'],
    ['#F2EC9B', '#1803A5'],
    ['#68A4A5', '#D9DAD9'],
    ['#DF3C5F', '#224193'],
    ['#E17888', '#1C5789'],
    ['#AE3B8B', '#E17888'],
    ['#EDC400', '#B25690'],
    ['#71B379', '#EDC400'],
    ['#D8D0CD', '#DF5587'],
    ['#355952', '#EAB63E'],
    ['#FAF6E7', '#355952'],
    ['#ED3224', '#F4F7F7'],
    ['#FBF5AA', '#735DA5'],
    ['#A8DACD', '#7A4D9F']
]
const texts = ['Trink einen Schluck.',
    'Trink zwei Schlücke.',
    'Trink drei Schlücke.',
    'Trink vier Schlücke.',
    'Trink fünf Schlücke.',
    'Verteile einen Schluck.',
    'Verteile zwei Schlücke.',
    'Verteile drei Schlücke.',
    'Verteile vier Schlücke.',
    'Verteile fünf Schlücke.',
    'Trink einen Shot.',
    'Verteile einen Shot.',
    'Trink einen Schluck Wasser! #stayhydrated',
    'Trink zwei Schlücke Wasser! #stayhydrated',
    'Trink drei Schlücke Wasser! #stayhydrated',
    'Trink vier Schlücke Wasser! #stayhydrated',
    'Trink fünf Schlücke Wasser! #stayhydrated',
    'Verteile einen Schluck Wasser! #stayhydrated',
    'Verteile zwei Schlücke Wasser! #stayhydrated',
    'Verteile drei Schlücke Wasser! #stayhydrated',
    'Verteile vier Schlücke Wasser! #stayhydrated',
    'Verteile fünf Schlücke Wasser! #stayhydrated',
    'Verteile und trinke einen Schluck oder verteile zwei, trinke dafür aber vier.',
    'Trinke zwei Schlücke, wenn du nicht weißt wer Tom Holland ist.',
    'Du bist der Daumenmaster. Wenn du deinen Daumen auf den Tisch legst, müssen alle es dir nachtun. Der:Die letzte trinkt.',
    'Alle Fußballfans trinken einen Schluck.',
    'Sage einen Zungenbrecher auf und verteile fünf Schlücke bei Erfolg; oder trink zwei.',
    'Denk dir eine Regel mit einer angemessenen Strafe aus.',
    'Kopf oder Zahl gegen eine Person deiner Wahl. Es geht um zwei!',
    'Bestimme deinen Trinkbuddy!',
    'Jeder mit Schuhen teurer als 80€ trinkt einen Schluck. Angeber!',
    'Stimme ein Lied an. Die letzte Person die mitsingt, trinkt.',
    'Poste einen Gruppenself... Ne, Spaß. Trinkt einfach.',
    'Alle mit der gleichen Oberteilfarbe wie Du trinken.',
    'Prüfe die Socken der Anderen. Alle, die keine Motivsocken anhaben, trinken zwei.',
    'Bestimme eine Person, die einen Schluck Alkohol und einen Schluck Wasser verteilt.',
    'Mache einen coolen Handschlag mit einer Person und ihr beide trinkt auf die Freundschaft.',
    'Trinke, wenn du schonmal die Unterschrift deiner Eltern gefälscht hast.',
    'Mache fünf Liegestütze oder trink fünf Schlücke.',
    'Verschenke eine Runde Immunität.',
    'Setz\' eine Runde mit Trinken aus.',
    'Trinke Wasser bis du wieder dran bist.',
    'Aktiviere die Anderen für eine Shotrunde.',
    'Erzähle einen Fun-Fact über dich und verteile einen Schluck.',
    'Spielregel und Strafe auf deinen Nacken!',
    'Verteile zwei, wenn du das Jugendwort des Jahres 2021 kennst. Sonst trink drei.',
    'Verteile drei, wenn du das Jugendwort des Jahres 2020 kennst. Sonst trink zwei.',
    'Verteile vier, wenn du das Jugendwort des Jahres 2019 kennst. Sonst trink einen.',
    'Schenke allen einen Schluck, die deine Lieblingsserie noch nie gesehen haben.',
    'Wenn du mit geschlossenen Augen deine Nasenspitze triffst, trink zwei. Ansonsten trink Wasser.',
    "Wenn du mit ausgestreckten Armen deine Zeigefinger über'm Kopf zusammenführen kannst, trink 3. Ansonsten trink Wasser.",
    'Wer als letztes aufsteht und "Schubidubi- wapwap" ruft, trinkt vier.',
    'Verteile einen Schluck für jeden Disney Animations-Film, den du gesehen hast. Allerdings nicht mehr als 10.',
    'Verteile einen Schluck für jede Marvel-Serie oder jeden Film, den du gesehen hast.  Allerdings nicht mehr als 10.',
    'Schenke jeder Person einen Schluck, die Pokemon Go gespielt hat.',
    'Gib einer Personen einen Kuss auf die Wange und mach ihr ein Kompliment. Trinken ist optional.',
    'Hebe eine Regel auf. Wenn es keine gibt, verteile 5 Schlücke.',
    'Trinke einen Shot und stelle eine Regel auf oder mach nichts.',
    'Erzähle dem Rest der Gruppe von deinem Lieblings-YouTube-Video und trink zwei.',
    'Schenke einer Person einen Schluck. Diese bedankt sich natürlich bei dir für die nette Geste.',
    'Alle, die die gleichen Buchstaben im Vornamen haben wie Du trinken einen Schluck pro gleichem Buchstaben.',
    'Alle mit der gleichen Haarfarbe wie du trinken einen Schluck.',
    'Gucke eine Person böse an und verteile zwei sehr ernste Schlücke.',
    'Rieche an einer Person. Wenn sie zu betrunken riecht, lass sie etwas Wasser trinken, sonst Alkohol.',
    'Sag ein Wort ohne K und verteil einen Schluck. Ansonsten trinke 5.',
    'Alle brillentragenden Personen verteilen 1 Schluck, aber nicht an dich.',
    'Trinke einen Schluck und beleidige eine Person oder mache ein Kompliment und verteile 1.',
    'Trinke ab jetzt nur noch mit deiner schwachen Hand.',
    'Lass alle Personen, die ein Glas umgeworfen haben etwas Wasser trinken.',
    'Sag ein Zitat aus Herr der Ringe und verteile einen Schluck.',
    'Verteile einen Schluck für jede Hauptperson aus Herr der Ringe, die Gefährten, die du aufzählen kannst.',
    'Verteile einen Schluck für jedes Spiel, dass du zu 100% durchgespielt hast.',
    'Wenn du jetzt dein Glas austrinkst, erhälst du Immunität für zwei Runden.',
    'Wenn du jetzt ein Glas Wasser trinkst, erhälst du Immunität für eine Runde.',
    'Alle Personen mit der gleichen Lieblingsband wie Du (und Du), verteilen zwei Schlücke.',
    'Wenn du in 10 Sekunden 3 Rapper nennen kannst, verteile zwei Schlücke, ansonsten trinke einen.',
    'Trinke und verteile einen Shot.',
    'Shotrunde? Shotrunde!',
    'Verteil einen Schluck an alle unter 25. Die können es ab.',
    'Sag einen Trinkspruch auf und trink mit allen einen Schluck.',
    'Stoße mit jeder Person an und trinke mit ihr persönlich einen Schluck.',
    'Wenn du einen Superheldenfilm außerhalb vom Marvel und DC nennen kannst, verteile vier.',
    'Wenn du schon einmal in einem Club namens Oberbayern warst, trink einen.',
    'Mach ein Lied deiner Wahl an und trinke einen.',
    "Mach »Hey, Wir Woll'n Die Eisbär'n Sehn« an und verteile zwei Schlücke.",
    'Halte mit einer Person für drei Runden Händchen. Falls ihr das nicht schafft, trinkt!',
    'Sitze, bis dein Getränk leer ist auf dem Boden. Sitzt du bereits auf dem Boden, setz dich woanders hin.',
    'Trinke einen Schluck für jeden Rechtshreibfehler, der dir aufgefallen ist.',
    'Mache ein Tiergeräusch nach oder trink 3.',
    'Du bekommst eine »Komme aus dem Gefängnis frei«-Karte, mit der du dich oder andere vom Aussetzen befreien kannst.',
    'Du bekommst eine Spiegel-Karte, womit du einmal Schlücke gegen dich zurückwerfen kannst.',
    'Mache acht Kniebeugen oder trinke fünf.',
    'Mache das Spiel durch eine innovative Regel unglaublich spannend!',
    'Mache einer Person ein Kompliment und schenke ihr einen Shot.',
    'Hebe eine Regel auf oder trink drei. Mache dich damit eventuell unbeliebt.',
    'Verteile einen Schluck für jedes Buch, dass du in der Schule auch ehrlich gelesen hast. Es lohnt sich doch!',
    'Wenn du deinen Ellenbogen ablecken kannst, verteile fünf. Sonst trink einen Schluck.',
    'Du bekommst die FUCK-YOU-Karte. Zeige einer Person den Mittelfinger und verdoppel ihre Schlücke.',
    'FLUCH! Immer wenn du dran bist, musst du eins Trinken.',
    'Verteile einen Schluck für jede Pflanze in deinem Zimmer.',
    'Spende einen Euro für einen guten Zweck oder trinke einen Shot.',
    'Verteile einen Schluck für jedes Konzert, bei dem du dieses Jahr warst. Falls immer noch Corona ist: I am sorry :c',
    'Zwei Runden Regelimmunität für dich!',
    'Spielt eine Runde Kommando Pimmperle. Wer gewinnt, verteilt fünf oder macht eine Shotrunde.',
    'Schere, Stein, Papier, Echse, Spock gegen eine Person deiner Wahl!',
    'Alle, die noch Facebook haben, trinken drei.',
    "Sag das ABC rückwärts auf. Bei Erfolg verteile acht, sonst trink zwei und einmal Wasser.",
    'Trinke einen Schluck für jedes Mal geblitzt werden in diesem Jahr.',
    'Trinke zwei Schlücke, wenn du Google nutzt. Verteile vier, wenn du Ecosia nutzt. Du darfst jetzt auch noch wechseln.',
    'Umarme eine Person und trinke mit ihr drei.',
    'Bestimmte eine Person, die eine Anzahl Schlücke und eine Person bestimmt, die die Schlücke verteilt.',
    'Verteile dein Alter geteilt durch 4 aufgerundet als Schlücke.',
    'Wenn du in einem Spiel mehr als 2000h hast, trinke zwei Schlücke.',
    'Drehe dich acht mal im Kreis. Dann trinke einen Shot, mach eine Runde Pause und trinke ein Glas Wasser.',
    'Wenn es vor 20 Uhr ist, trinkt eine Shotrunde. Wenn es vor 22 Uhr ist drei Schlücke, danach Wasser.',
    'Trinke was von deinem Getränk ab und fülle es mit Softdrink auf. Wahrscheinlich war die Mische eh zu krass.',
    'Trinke zwei, außer wenn dein Alter in der Fibonacci-Reihe liegt.',
    'Trinke zwei, wenn du eine Halskette, Ohrringe, Uhr oder andere Schmuckstücke trägst.',
    'Verteile einen Schluck für jede Sitcom, die du geguckt hast.',
    'Spiele eine Runde Todespyramide und verteile ein Glas auf Ex oder skip die Aufgabe.',
    'Du bist der Wikinger. Wenn du mit deinen Händen einen Helm machst, müssen alle anfangen zu rudern. Der Faulbär trinkt, die Sau.',
    'Trinke für fünf Runden immer einen weniger als du müsstest.',
    'Trinke für fünf Runden immer einen mehr als du müsstest.',
    'Verteile die Anzahl der Buchstaben deines Nachnamens.',
    'Identifiziere alle Leute, die blond sind. Jetzt hast du eine Liste mit blonden Leuten, die völlig wertlos ist. Trink einen.',
    'GEWINNE GEWINNE GEWINNE, TRINKE EINS VERTEILE ZWEI.',
    'Lasse einen Person einen Zungenbrecher deiner Wahl aufsagen. Schafft sie es nicht, trinkt sie drei.',
    'Wenn du ein Gedicht aufsagen kannst, verteile einen Schluck pro Strophe. Ansonsten trink drei.',
    'Verteile zwei, wenn du einen Song von Avicii anstimmen kannst.',
    'Du bist eine Runde stumm.',
    'Suche eine Person aus, die eine Runde stumm ist.',
    'Lasse eine Person zwanzig Schlücke trinken. Pro Liegestütz oder Sit-Up kann die Schluckzahl reduziert werden.',
    'Alle Personen, die in einem Fitnesstudio angemeldet sind, trinken zwei Schlücke.',
    'Bringe der Runde eine non-offensive Beleidigung (du vergammelte Mandarine) bei und trinke einen.',
    'Erzähle den anderen bis ins kleinste Detail, wie du angereist bist und trinke zwei.',
    'REGELZEIT FREUNDE! Oder heb eine Regel auf, wenn es schon mehr als drei gibt.',
    'Sag ein Wort. Wer als erstes keinen Reim hat, trinkt drei.',
    'Sag ein Wort. Die nächste Person macht ein neues Wort daraus. Bsp: Haus->Wand. Wer kein Wort bilden kann, trinkt zwei.',
    'Verteile einen Schluck für jede:n alte Lehrer:in, die du noch aufzählen kannst.',
    'Alle, die ein Auto haben, trinken einen Schluck.',
    'Verteile einen Schluck, wenn du eine andere Tastenkombination als Strg+C, Strg+V oder Strg+X nennen kannst.',
    'Bestimme eine Person, die ein Wort deiner Wahl buchstabieren muss. Schafft sie es nicht, trinkt sie drei.',
    'Verteile einen Shot an ein Pärchen, beste Freund:innen, Geschwister. Irgendwas im Paar halt.',
    'Wenn du alle Harry-Potter-Teile in richtiger Reihenfolge aufsagen kannst, verteile vier, ansonsten trink zwei.',
    'Schließt alle die Augen und haltet eine Zahl von eins bis fünf vor euren Kopf. Öffnet die Augen. Gemeinsame Zahlen trinken.',
    'Wähle eine Person. Diese entscheidet, wie viele du trinken darfst.',
    'Berühre dein Knie mit deiner Stirn und verteile zwei. Schaffst du es nicht, trinke zwei.',
    'Erzähle von einem weirden Date und trinke zwei, um zu vergessen.',
    'Alle trinken eine Zahl von 1 bis 5. Du entscheidest. Mit großer Macht kommt große Verantwortung.',
    'Trink zwei, wenn du schonmal auf Twitch gestreamt hast.',
    'Verteile 1 für jede Teesorte in deinem Schrank.',
    'Tausche mit einer anderen Person den Namen. Falls ihr falsch reagiert, müsst ihr zur Strafe trinken.',
    'Wenn du Raucher bist: Trinke vier. Ansonsten verteile vier.',
    'Fordere eine Person zu einem Anstarrbattle raus. Ihr entscheidet den Einsatz.',
    'Zeige der Runde das letzte Foto, was du gemacht hast oder trinke vier Schlücke',
    'Wenn du nicht weißt wer Till Lindemann ist, trinke vier Schlücke.',
    'Du bekommst eine Cleanse-Karte, mit der du irgendwann eine Regel entfernen kannst.',
    'Kategoriespiel! Du bestimmst die Kategorie, bspw. Zigarettenmarken.',
    'Spielt eine Runde »Ich packe meinen Koffer«. Du fängst an.',
    'Stelle einer Person eine Subtraktionsaufgabe mit zwei Zahlen zwischen 0 und 100. Zeit: 10sek; Strafe: zwei.',
    'Stelle einer Person eine Additionsaufgabe mit zwei Zahlen zwischen 0 und 100. Zeit: 10sek; Strafe: drei.',
    'Stelle einer Person eine Multiplikationsaufgabe mit zwei Zahlen zwischen 0 und 10. Zeit: 10sek; Strafe: zwei.',
    'Wähle einen Trinkbuddy für zwei Runden. Falls ihr Euch danach noch versteht, dürft ihr auch Trinkbuddies bleiben.',
    'Du bekommst eine Urlaubskarte. Du darfst bei Bedarf zwei Runden aussetzen.',
    'Du bekommst eine My-Body-Is-My-Temple-Karte. Du darfst einmal deine Schlücke gegen Wasser austauschen.',
    'Du bekommst die NOPE-Karte. Du darfst irgendwann irgendeine Aufgabe, Regel oder andere Karte negieren.',
    'Alle über 25 trinken einen Schluck Wasser gegen den Kater am morgen.',
    'Trinke einen Schluck, wenn du schonmal illegale Drogen konsumiert hast.',
    'Trinke zwei Schlücke, wenn du schonmal (aus Versehen) was geklaut hast.',
    'Desinfiziere die Hände der Person links zu dir. Hast du kein Desi dabei, besorg dir welches!',
    'Gib einer Person deiner Wahl eine Begrüßungs-Faust, dann trinkt jeweils drei Schlücke und verteilt gemeinsam vier.',
    'Zeig der Runde eine unnütze Webseite, die du allerdings viel zu oft besuchst. Hast Du keine, good for you!',
    'Gleiche deinen Getränk-Füllstand mit der Person rechts von dir an. Entweder durch austrinken oder auffüllen.',
    'Bestimme eine Person, diese bekommt von allen den Kopf gestreichelt. :)',
    'Verteile drei Schlücke oder sei eine Runde immun. Deine Wahl!',
    'Berühre die Nase einer Person und sage "Boop". Dann trinkt ihr beide zwei Schlücke.',
    'Alle »Fallout Boy«-Fans trinken einen Schluck und verteilen zwei!',
    'Alle »Electric Callboy«-Fans trinken einen Schluck und verteilen zwei!',
    'Wahrheit oder Pflicht mit einer Person deiner Wahl. Wenn diese Person einen Shot trinkt, kann sie sich entziehen.',
    'Alle trinken eine Runde lang mit der schwachen Hand.',
    'Sage einen Zungenbrecher auf und verteile einen Shot bei Erfolg. Sonst trink drei.',
    'Du kannst eine Runde alle Schlücke verteilen.',
    'Benenne das Rezept von Remus Feinster oder trink drei Schlücke.',
    'Fange ein Zitat / Insider an und die letzte Person die mitspricht trinkt 3.',
    'WASSERFALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL!!!!',
    'Du darfst eine Person zum Busfahren (5 Karten) mitnehmen. Wenn du keine Ahnung hast was das heißt, umarmt euch und trinkt 4.',
    'Tausche mit einer Person ein Kleidungsstück deiner Wahl.',
    'Ziehe ein Kleidungsstück deine Wahl aus. Eine Socke ist zu wenig tho!',
    'Lasse jemanden 1 Runde aussetzen oder einen Shot trinken.',
    'Setze eine Runde aus!',
    'Gebt euch der Reihe nach im Uhrzeigersinn ein Kompliment und trinkt auf die Freundschaft.',
    'Wasserfall, aber mit Umarmungen. Trinken ist im Anschluss optional.',
    'Setze mit einer Person eine Runde aus, indem ihr euch die ganze Zeit umarmt.',
    'Du bekommst eine »Ab in die Stille Ecke« Karte, mit der du eine Person für schlechte Witze bestrafen kannst.',
    'Du bist vergiftet. Trinke jetzt 3, in der nächsten Runde 2 und dann 1.',
    'Vergifte eine Person deiner Wahl mit 3 Schlücken. (Sie trinkt jetzt 3, 2, 1 über 3 Runden)',
    'Vergifte eine Person deiner Wahl mit 5 Schlücken Wasser. (Sie trinkt jetzt 5, 4, 3, 2, 1 über 5 Runden)',
    'Du bekommst eine Gegengift-Karte, mit der du eine vergiftete Person retten kannst.',
    'Du bekommst eine »No U« Karte, mit der du eine Person ihre verteilten Schlücke trinken lassen kannst.'
] //» «
let round = 1
let subround = 0

changeColor()
if(Cookie.get('names') === null) {
    Cookie.set('names', [''])
}
for(const name of Cookie.get("names")) {
    addName(name)
}

$nameInput.keypress((e) => {
    //Enterpress bei leerem Input
    if (e.which === 13 && $nameInput.val() !== '') {
        addName($nameInput.val())
        $startButton.removeClass('hidden')
    }
});

$(window).resize(_ => {
    moveProgressBar();
});

//Start Game
$startButton.on('click', () => {
    startGame();
})

//BlurEffect on Enter
$('#name_button').on('click', () => {
    addName($nameInput.val())
    $nameInput.blur()
})

//Adds name to array
function addName(name) {
    name = name.trim()
    if(!name) return
    if(names.includes(name)) return
    names.push(name)
    $names.append(`<div class="name">${name} | <span class="delete_name">x</span></div>`)
    $nameInput.val('')
    $startButton.removeClass('transparent')
    Cookie.set("names", names)
    //Delete Names
    $('.delete_name').on('click', (e) => {
        name = e.currentTarget.parentElement.textContent // will be <name> | x
        name = name.slice(0, name.length - 4) // cut off " | x"
        // when the name is not found return. if we don't do this we will delete the last entry of the array
        if(names.indexOf(name) === -1 ) return
        names.splice(names.indexOf(name), 1) //delete name from array
        $(e.currentTarget.parentElement).remove()
        if(names.length === 0) {
            $startButton.addClass('transparent')
        }
        Cookie.set("names", names)
    })
}

//Starts Game
function startGame() {
    //Clear Playground
    $startCard.addClass('hidden')
    $gameCard.removeClass('hidden')
    for(name of names){
        players[name] = 1;
    }
    //Random-Card-Selection
    draw()
    //Click-Event for next card
    $gameCard.on('click', () => {
        draw()
    })
    //Next Card with Spacebar
    $('html').keypress((e) => {
        if(e.which !== 32) return;
        draw()
    })
}

function draw() {
    subround += 1;
    if(subround > names.length){ // we start counting at one and doing ">" to fill up the bar to 100%
        round += 1;
        subround = 1;
        $('#title-number').text(round);
    }
    moveProgressBar()
    redrawCard()
    changeColor()
}

//Colorchanger
function changeColor() {
    const colorPair = colorpairs[Math.floor(Math.random() * colorpairs.length)]
    $html.css('--primary', colorPair[0])
    $html.css('--secondary', colorPair[1])
}

//Select random target
function getRandElement(ele) {
    return ele[Math.floor(Math.random() * ele.length)]
}

function redrawCard() {
    $cardTitle.text(selectPlayer());
    $cardText.text(getRandElement(texts));
}

// Pseudo RNG: The more often a player is selected, the less likely it will be to select him*her once again
function selectPlayer(){
    let playerSum = 0.0
    for(const player in players){
        playerSum += 1 / players[player]
    }
    const randomFloat = getRandomFloatInclusive(0, playerSum)
    let runningSum = 0.0
    for(const player in players){
        runningSum += 1 / players[player]
        if(runningSum > randomFloat){
            players[player] += 1
            return player
        }
    }
    console.log('Critical Error in population.selectPlayer()')
}

function getRandomFloatInclusive(min, max) {
    return Math.random() * (max - min) + min; //The maximum is inclusive and the minimum is inclusive
}

function moveProgressBar() {
    $('.progress-bar').stop().animate({
        top: subround / names.length * $('.progress-wrap').height()
    }, 1000);
}
