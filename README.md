# Never Trust Picolo
I was fed up with drinking games. Some of them were overly sexist. Some of them relied to much on drinking without being fun.
Some of them weren't usable on a phone. And some of them did cost a little to much money.

So I took 36h to invent my own little drinking game. It is not perfect and only available in German, but I like it.

![Start Screen](./preview1.png "Start Screen")
![Game Card](./preview2.png "Game Card")


It contains:
- A start screen to edit names.
- The names are saved via cookie, so you can just refresh the page to edit the names.
- A lot of different cards. 
- A number and progress bar to keep tracks of the rounds.
- A semi-random algorithm to pick players and cards.
- Random colors for ✨design✨.
- Some incentives to drink water.

## Free
If you want, you can make your own drinking game out of this. But don't sell it!
