export default class Cookie {
    static set(name, levels){
        const levelsString = JSON.stringify(levels)
        const currentDate = new Date()
        const experationDate = new Date(currentDate.getTime() + 94608000000); // 3 Jahre
        document.cookie = `${name}=${levelsString};${experationDate.toUTCString()};path=/`
    }

    static get(name){
        const cookieName = name + '='
        const decodedCookie = decodeURIComponent(document.cookie);
        const cookies = decodedCookie.split(';')
        for(let cookie of cookies){
            cookie = cookie.trim()
            if(cookie.indexOf(cookieName) === 0){
                return JSON.parse(cookie.substring(cookieName.length))
            }
        }
        return null
    }
}
